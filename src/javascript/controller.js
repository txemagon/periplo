/* class IController
 * Interface Controller
 *
 * Coordinates canvas and the artificial intelligence that
 * gathers input.
 */

function IController(can_name){
    this.menu = new MenuController ()
     this.wwindow = new WWindow(can_name)
     this.ai = new AI()
     var casters = [this.wwindow, this.ai, this.menu]
     for (var c of casters)
        c.parent = this

     this.model = new TrackHandler()
     this.render_path = this.model.render(1)  // todo:  Parameter shall change depending on scale

    this.active_object
}

/* IController#set_active_object
 *
 * Enables a decorator around active object
 */
IController.prototype.set_active_object = function (num) {
    this.active_object = num
    this.wwindow.set_decorator (num, this.get_window_coords(this.model))
    if (num) {
        var p = this.model.get_point (this.active_object)
        if (p)
            this.menu.load ( {id: this.active_object, class: p.constructor.name} )
    }
    else
        this.menu.clear ()
}

/* IController#invalidate_render
 *
 * Generates a new render path, when a point is added or changes
 */
IController.prototype.invalidate_render = function () {
    this.render_path = this.model.render (1)  // todo: parameter shall change depending on scale
}

/* IController#get_renderpath
 *
 * Returns cached render path
 */
IController.prototype.get_render_path = function () {
    return this.render_path
}

/* Icontroller.get_window_coords
 *
 * Translates real world coordinates to window coordinates
 */
IController.prototype.get_window_coords = function (points) {
    return this.model.scale_copy (this.wwindow.map_image.scale)
}

/*
 * IController#invalidate_window ()
 *
 * Sets the need or repainting the window
 **/
IController.prototype.invalidate_window = function (temp_pan, temp_scale) {
    this.wwindow.repaint (this.get_window_coords (this.model), temp_pan, temp_scale, this.get_render_path())
}

/*
 * IController#change_point_class
 */
IController.prototype.change_point_class = function (idx, prop_out, prop_in ) {
    this.model.change_class (idx,prop_out, prop_in)
    this.invalidate_render ()
}

/*
 * IController#defaault_create_point
 *
 * Creates new point. The first one is to be an entrance point,
 * the last a one an output one while the others are defaultted in-out.
 */
IController.prototype.default_create_point = function (x, y) {
    // log ("Pushing (" + x + ", " + y + ")")
    var Cl = PointIn
    if (!this.model.length)
        Cl = PointOut
    var np = new Cl (x, y)
    this.model.push (np)
    if (this.model.length > 2)
        this.change_point_class (
            this.model [this.model.length-2].id,
            "PointOut", "PointIn")

    this.invalidate_render()
    return np
}

/* IController.rcv
 *
 * Handles event messages from canvas and ai.
 */
IController.prototype.rcv = function (mssg, sender) {
    switch (mssg.name.toLowerCase()){
        case "new point":
            if (this.active_object) {
                this.set_active_object ()
                return
            }
            if (mssg.toSource)
                log (mssg.toSource())
            var k = this.wwindow.map_image.scale
            var s = this.wwindow.scale
            var p = mssg.data
            var x = p.x
            var y = p.y
            x /= s
            x += this.wwindow.pan.x0
            x /= k
            y /= s
            y += this.wwindow.pan.y0
            y /= k
            var np = this.default_create_point (mssg.data.x, mssg.data.y)
            this.set_active_object (np.id)
            break
        case "pan canvas":
            this.set_active_object ()
            this.wwindow.move (mssg)
            break
        case "zoom":
            this.set_active_object ()
            this.wwindow.zoom (mssg)
            break
        case "edit point":
            this.set_active_object ( mssg.data.id )
            break
        case "property change":
            if (!this.active_object)
                return
            this.change_point_class (this.active_object, mssg.data.pointout, mssg.data.pointin)
            this.set_active_object (this.active_object)
            break
    }
    this.invalidate_window ()
}
