/**
 * @class Display
 *
 * Handle only HTML Display
 */
function Display(name) {
    this.map = new Bmap("base_map")
    this.width
    this.height
    this.name = name
    this.svg_layer
    var that = this
    this.svg_elem = []
    this.svg_path = []
    this.decorated      // id of the element to be decorated.

    function initialize() {
        var display = document.getElementById(name)
        if (!display)
            throw "Impossisble to inititate display"
        that.width = display.offsetWidth
        that.height = display.offsetHeight

        that.svg_layer = document.getElementById ("svg-layer")
        that.svg_layer.setAttribute ("viewBox", "0 0 " + that.width + " " + that.height)
        if (!that.svg_layer)
            throw ("svg-layer not found.")
    }

    if (typeof (name) !== "undefined")
        initialize()

}

/*
 * Display#set_decorator
 */
Display.prototype.set_decorator = function (num) {
    this.decorated = num
}

/* clear_display: Erases associated display.
 */
Display.prototype.clear_display = function () {
    this.svg_layer.innerHTML = ""
}

Display.prototype.dump = function () {
    for (var i=0; i<this.svg_elem.length; i++)
        this.svg_layer.appendChild (this.svg_elem[i].get_svg())  // Browser won't update if you're still dragging
    for (var i=0; i<this.svg_path.length; i++)
        this.svg_layer.appendChild (this.svg_path[i].get_svg())
}

Display.prototype.refresh = function () {
    this.clear_display ()
    this.dump ()
}

Display.prototype.repaint  = function (pan, scale, model, regenerate_data, path) {

    if (!model)
        return

    /* Recreate Graphical elements */
    if (regenerate_data) {
        this.svg_elem = []
        /* Generate control points */
        for (var i=0; i<model.length; i++) {
            var svg_new = new (SVG.mapper (model[i]))(
                (model[i].x - pan.x0) * scale,
                (model[i].y - pan.y0) * scale, 10
            )
            svg_new.get_svg ().setAttributeNS(null, "data-id", model[i].id)
            svg_new.get_svg ().setAttributeNS(null, "id", model[i].id)
            svg_new.get_svg ().setAttributeNS(null, "class", model[i].constructor.name)
            svg_new.get_svg().setAttributeNS(null, "stroke", "white")

            if (this.decorated == model[i].id)
                svg_new.get_svg().setAttributeNS(null, "stroke-width", "2")
            else
                svg_new.get_svg().setAttributeNS(null, "stroke-width", "1")


            this.svg_elem.push (svg_new)
        }

        /* Draw Path */
        if (path) {
            this.svg_path = []
            for (var i=0; i<path.length-1; i++) {
                var svg_new = new SVGLine(
                    (path[i].x - pan.x0) * scale,
                    (path[i].y - pan.y0) * scale,
                    (path[i+1].x - pan.x0) * scale,
                    (path[i+1].y - pan.y0) * scale
                )

                this.svg_path.push (svg_new)
            }
        }
        this.refresh ()


        /* Just move what is present */
    } else {
        for (var i=0; i<this.svg_elem.length; i++) {
            var svg = this.svg_elem[i].get_svg()
            var m = model[i]
            var x = (m.x - pan.x0) * scale
            var y = (m.y - pan.y0) * scale
            svg.setAttributeNS (null, "cx", x)    // Browser doesn't seem to refresh while still dragging
            svg.setAttributeNS (null, "cy", y)
        }
    }
}


