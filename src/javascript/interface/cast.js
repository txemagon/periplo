/* Interface/Module Cast
 *
 * Whatever class/object implements Cast
 * gains a cast method already linked with
 * its parent / owner to cast to.
 */
function Cast (){}

Cast.prototype.cast = function (mssg) {
    this.parent.rcv (mssg, this)
}
