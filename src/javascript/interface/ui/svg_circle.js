/*
 * SVGCircle class
 */


// SVG Inheritance
SVGCircle.prototype = new SVG
SVGCircle.prototype.constructor = SVGCircle
SVGCircle.prototype.super = SVG

/* SVGCircle constructor */
function SVGCircle (cx, cy, r) {

    var args = Array.prototype.slice.call(arguments)

    if (!args.length)
        return                       // Now inheriting

    SVG.call (this, 'circle') /* Delegate Base class constructor */
    this.item.setAttributeNS(null, "cx", cx)
    this.item.setAttributeNS(null, "cy", cy)
    this.item.setAttributeNS(null, "r", r)
}

function SVGPointIn (cx, cy, r) {
   SVGCircle.apply (this, arguments)
   this.item.setAttributeNS (null, "fill", SVG.landmark_color[this.constructor.name])
}

function SVGPointOut (cx, cy, r) {
   SVGCircle.apply (this, arguments)
   this.item.setAttributeNS (null, "fill", SVG.landmark_color[this.constructor.name])
}

function SVGPointInOut (cx, cy, r) {
   SVGCircle.apply (this, arguments)
   this.item.setAttributeNS (null, "fill", SVG.landmark_color[this.constructor.name])
}

function SVGPointSkip (cx, cy, r) {
   SVGCircle.apply (this, arguments)
   this.item.setAttributeNS (null, "fill", SVG.landmark_color[this.constructor.name])
}

var classes = [SVGPointIn, SVGPointOut, SVGPointInOut, SVGPointSkip]
derive_from (classes, SVGCircle)
