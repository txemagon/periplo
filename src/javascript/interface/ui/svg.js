/* SVG Parent Class
 */

SVG.type = {
    'circle': 'http://www.w3.org/2000/svg',
    'line': 'http://www.w3.org/2000/svg'
}

SVG.landmark_color = {
    SVGPointIn: "crimson",
    SVGPointOut: "darkblue",
    SVGPointInOut: "darkolivegreen",
    SVGPointSkip: "yellow"
}

/* Map model points into SVG classes */
SVG.mapper = function (point) {
    return eval ("SVG" + point.constructor.name )
}

function SVG (type) {
    if (!type)
        return
    this.item = document.createElementNS(SVG.type[type], type);
}


SVG.prototype.get_svg = function () { return this.item }
