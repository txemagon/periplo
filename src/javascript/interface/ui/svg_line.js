/*
 * SVGLine class
 */


// SVG Inheritance
SVGLine.prototype = new SVG
SVGLine.prototype.constructor = SVGLine
SVGLine.prototype.super = SVG

/* SVGLine constructor */
function SVGLine (x1, y1, x2, y2) {

    var args = Array.prototype.slice.call(arguments)

    if (!args.length)
        return                       // Now inheriting

    SVG.call (this, 'line') /* Delegate Base class constructor */
    this.item.setAttributeNS(null, "x1", x1)
    this.item.setAttributeNS(null, "y1", y1)
    this.item.setAttributeNS(null, "x2", x2)
    this.item.setAttributeNS(null, "y2", y2)
    this.item.setAttributeNS(null, "stroke", "darksalmon")
    this.item.setAttributeNS(null, "stroke-width", "4")
}


