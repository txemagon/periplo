/******************
 * CONSTANTS
 */

var map_scale = 1 //1/20

/**
 * World Window class
 *
 * Handle Pan and Zoom over the display.
 *
 * WWindow is a logical device with
 *   - width: 1
 *   - height: 1 * display.height / display.width
 *
 *   It maps to physical window handling pan and scale.
 *
 *   Stores a list of trackable graphical item coordinates (xmarks)
 *   referred to its logical coordinates.
 *
 *   WWindow is a listener of the html display region.
 **/

WWindow.prototype = new Cast
WWindow.prototype.constructor = WWindow

function WWindow (can_name) {
    this.scale = 1                // zoom level. 2 means: see twice bigger.
    this.pan = { x0: 0, y0:0 }    // Displacement over the map in map units
    this.display = new Display(can_name)

    this.map_image = {
        scale: map_scale
    }
}
/*
 * WWindow#set_decorator
 */
WWindow.prototype.set_decorator = function (num, model) {
    this.display.set_decorator(num)
    this.display.repaint (this.pan, this.scale, model, true)
}


/* Logical layer has changed and elements need to be repainted. */
WWindow.prototype.repaint = function (model, temp_pan, temp_scale, path) {
    var regenerate_data = typeof (temp_pan) == "undefined" && typeof (temp_scale) == "undefined"
    var pan = temp_pan || this.pan
    var scale = temp_scale || this.scale
    this.display.repaint(pan, scale, model, regenerate_data, path)
}

WWindow.prototype.move = function (mssg) {
    if (!mssg || !mssg.data || !mssg.data.x)
        return
    var x = mssg.data.x
    var y = mssg.data.y
    var set = mssg.data.set

    var dx = this.pan.x0 + x / this.scale // Displacement in the map image
    var dy = this.pan.y0 + y / this.scale // Not in display

    if (set) {
        this.pan.x0 = dx
        this.pan.y0 = dy
    }

    // watch ("Pan: [x0: " + dx + ", y0: "+ dy + "]")
    this.parent.invalidate_window({x0: dx, y0: dy})     // Browser won't update if you're still dragging.
}

WWindow.prototype.zoom = function (mssg) {
    if (!mssg || !mssg.data || !mssg.data.x)
        return
    var x = mssg.data.x
    var y = mssg.data.y
    var k = mssg.data.scale


    this.pan.x0 += x / this.scale * (1 - 1 / k)
    this.pan.y0 += y / this.scale * (1 - 1 / k)

    this.scale *= k       // Scale slightly drifts due to rounding

    // watch ("Scale: [" + this.scale + "]")
    this.parent.invalidate_window(this.pan, this.scale)
}

WWindow.prototype.get_active = function (path) {
    return this.parent.get_render_path ()
}
