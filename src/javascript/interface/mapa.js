
var baseMap
var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
var osm

/* class Bmap
 * Map creation
 *
 * Map with default coordinates,
 * OpenStreetMap filter.
 */

function Bmap(map_name){
    baseMap = L.map(map_name, {
        fadeAnimation: true,
        zoomAnimation: true,
        markerZoomAnimation: true
    }).setView([40.484673, -3.365936], 12)

    osm = new L.TileLayer(osmUrl, {
        minZoom: 5, maxZoom: 16,
        attribution: osmAttrib,
        updateWhenIdle: true,
        reuseTiles: true
    });
    osm.addTo(baseMap)
}
