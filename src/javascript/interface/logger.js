var logpaper
var watchpaper

function log(mssg) {
    logpaper = logpaper || document.getElementById("output_dbg_log")
    var d = new Date()
    logpaper.innerHTML = d.getHours() + ":" + d.getMinutes() + ":" + d.getSeconds() + ": " + mssg + "<br/>\n" + logpaper.innerHTML
}

function watch(mssg) {
    watchpaper = watchpaper || document.getElementById("output_dbg_cur")
    watchpaper.innerHTML = mssg
}
