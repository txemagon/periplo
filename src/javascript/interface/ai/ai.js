/**
 * Handle user entries. Can work as a state machine if needed.
 */

var display_name ="display"
var active_layer="svg-layer"


AI.prototype = new Cast
AI.prototype.class = AI
function AI () {
    this.mouse_state = {
        clicked: false,
        x0: 0,
        y0: 0,
        x:  0,
        y:  0
    }

    register_event (this, display_name, "click", AI.prototype.input_canvas)
    register_event (this, display_name, "mousedown", AI.prototype.input_canvas)
    register_event (this, display_name, "mouseup", AI.prototype.input_canvas)
    register_event (this, display_name, "mousemove", AI.prototype.input_canvas)
    register_event (this, display_name, "mouseout", AI.prototype.input_canvas)
    register_event (this, display_name, "wheel", AI.prototype.input_canvas_wheel)
}

AI.prototype.same_point = function () {
    return this.mouse_state.x0 == this.mouse_state.x &&
           this.mouse_state.y0 == this.mouse_state.y
}

AI.prototype.fire_pan_canvas = function (x, y, set) {
    set = set || false
    this.cast (
           { name: "Pan Canvas",
             data: {
                 x: this.mouse_state.x0 - this.mouse_state.x,
                 y: this.mouse_state.y0 - this.mouse_state.y,
                 set: set
             } }
        )
}

AI.prototype.input_menu = function (ev) {}

AI.prototype.input_canvas_wheel = function (ev) {
    var x = ev.offsetX
    var y = ev.offsetY
    var k = ev.deltaY > 0 ? 0.5 : 2 //0.9091 : 1.1
    this.cast ( {name: "Zoom", data: {x: x, y:y, scale: k} } )
}

AI.prototype.input_canvas = function (ev) {
    var x = ev.offsetX
    var y = ev.offsetY

    switch (ev.type) {
        case "click":
            if (ev.target.id == active_layer) {
                if (this.same_point ())
                    this.cast( {name: "New Point", data:{x: x, y:y}} )
            } else {
                var svg_el = document.getElementById (ev.target.id)
                var id = svg_el.id
                var cl = svg_el.classList[0]
                this.cast ( {name: "Edit Point", data:{id: id, class: cl}} )
            }
            break
        case "mousedown":
            this.mouse_state.clicked = true
            this.mouse_state.x0 = x
            this.mouse_state.y0 = y
            this.mouse_state.x  = x
            this.mouse_state.y  = y
            break
        case "mousemove":
            this.mouse_state.x = x
            this.mouse_state.y = y
            if (this.same_point ())
                return
            if (this.mouse_state.clicked)
                this.fire_pan_canvas (x, y)
            break
        case "mouseup":
            this.mouse_state.clicked = false
            this.mouse_state.x = x
            this.mouse_state.y = y
            if (this.same_point ())
                return
            this.fire_pan_canvas (x, y, true)
            break
        case "mouseout":
            if (this.mouse_state.clicked) {
                this.mouse_state.clicked = false
                this.mouse_state.x = x
                this.mouse_state.y = y
                this.fire_pan_canvas (x, y, true)
            }
            break
    }
}

