/* Listener contents:
 *
 * EXAMPLE:
 *      {fn: AI.prototype.input, ob: this}
 *      {fn: AI.prototype.input, ob: this, target: node}
 *      {fn: AI.prototype.input, ob: this, target: "target_id"}
 * */
var listeners = []

/**
 * listeners#push (singleton)
 *  Override Array#push to check data validity.
 *
 *  Accepts targe on string and HTMLElement
 */
listeners.push = function () {
    for (var i=0; i< arguments.length; i++) {
        if (
            typeof (arguments[i].fn) == "undefined"  ||
            typeof (arguments[i].ob) == "undefined"
        )
            throw "Invalid listener data."
        if ( arguments[i].target )
            if ( is_string (arguments[i].target) ){
                var node = document.getElementById (arguments[i].target)
                if (!node)
                    throw "Invalid target registering event listener."
                arguments[i].target = node
            } else if ( ! (arguments[i].target instanceof HTMLElement ) )
                throw "Event target is neither a node, nor a string. Impossible to register listener."

        Array.prototype.push.call (listeners, arguments[i])
    }
}


/* @method input
 *
 * Notifies html events to registerd listeners.
 */
function input(ev) {

    for (var i=0; i<listeners.length; i++)
        if (
            typeof listeners[i].target === 'undefined' ||
            listeners[i].target == ev.target
        )
            listeners[i].fn.call(listeners[i].ob, ev)
}

function main() {
    icontroller = new IController("display")
}

