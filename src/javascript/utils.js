function is_string( value ) {
    return typeof value === 'string' || value instanceof String
}

function is_number( value ) {
    return typeof value === 'number' || value instanceof Number
}

function register_event (obj, target, type, fn_name) {
    document.getElementById (target).addEventListener ( type, function (event) { return fn_name.call(obj, event) } )
}

function derive_from (classes, base_class) {
    for (let c of classes) {
        c.prototype = new base_class
        c.prototype.constructor = c
        c.prototype.super = base_class
    }
}

// Disable: privacy.file_unique_origin in about:origin for local testing
function async_load (page, container, optfunc) {
    var xhttp = new XMLHttpRequest ()
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById (container).innerHTML = this.responseText
            if (optfunc)
                optfunc ()
        }
    }

    xhttp.open ("GET", page, true)
    xhttp.send ()
}
