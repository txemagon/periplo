/*
 * MenuController Class
 */

MenuController.prototype = new Cast
MenuController.prototype.constructor = MenuController


function MenuController (){
    this.cm = document.getElementById ("contextual_menu")
    this.active_state = {}
}

/* MenuController#active_state
 *
 * Creates a new menu state
 */
MenuController.prototype.new_active_state = function () {
    this.active_state = {}
}

/* MenuController#change_active_state
 *
 * Changes menu's state
 */
MenuController.prototype.change_active_state = function (attr, value) {
    this.active_state[attr] = value
    this.cast ({
        name: "Property Change",
        data: { pointin: this.active_state.tanin, pointout: this.active_state.tanout }
    })
}

/* Load a Point form in the contextual menu */
MenuController.prototype.load = function (mssg) {
    this.clear ()
    switch (true){  // Testing with Regular Expressions
        case /^Point/.test(mssg.class):
            async_load ("./ui/menus/point_menu.xml", "contextual_menu",
                () => {
                    var tanout = document.getElementById ("c_tanout")
                    var tanin = document.getElementById ("c_tanin")
                    this.active_state.tanout = tanout.checked = Point.query ("out", mssg.class)
                    this.active_state.tanin = tanin.checked = Point.query ("in", mssg.class)
                    tanout.onclick = (ev) => {
                        this.change_active_state ("tanout", tanout.checked)
                    }

                    tanin.onclick = (ev) => {
                        this.change_active_state ("tanin", tanin.checked)
                    }

                }
            )
            break
    }
}

/* Clear contextual menu */
MenuController.prototype.clear = function () {
    this.cm.innerHTML = ""
    this.new_active_state ()
}
