/* class TrackHandler: inherits from Array */
TrackHandler.prototype = new Array
TrackHandler.prototype.constructor = TrackHandler
TrackHandler.prototype.super = Array

function TrackHandler ()
{

}

/* TrackHandler#push
 *
 * Pushes a new point to the track.
 *
 * PARAMETERS: (variadic)
 * points: single or multiple points to be added.
 * If a list is given and a single input is found
 * to be invalid, the whole operation is discarded.
 * */
TrackHandler.prototype.push = function (points) {
    var ok = true
    for (var i=0; i<arguments.length; i++)
        if (! (arguments[i] instanceof Point))
            ok = false
    if (!ok)
        throw "Invalid point. Could not add to track."

    Array.prototype.push.apply(this, arguments)
}

/* TrackHandler#render_spline
 *
 * Renders an spline segment between three points.
 *
 * PARAMETERS:
 *  p1 : Origin of spline.
 *  p2 : Middle point
 *  p3 : Final spline point
 * RETURNS:  Array of points.
 * */
TrackHandler.prototype.render_spline = function (p1, p2, p3, precision) {
    var line   = []
    var render = []

    line[0] = new Line (p1, p2)
    line[1] = new Line (p2, p3)

    for (var i=0; i<1; i+=precision)
        render.push ( (new line (line[0].at (i), line[1].at (i)) ).at (i) )

    return render 
}

/* TrackHandler#render
 *
 * Yields an array of points to render the track
 *
 * PARAMETERS:
 * RETURNS:  Array of points.
 * */
TrackHandler.prototype.render = function (points) {

    var rendered_spline = []
    for (var i=0; i<this.length-2; i+=2)
        rendered_spline.concat (TrackHandler.render_spline (this[i], this[i+1], this[i+2]))

}

/*
 * TrackHandler#get_point ()
 *
 * Returns a point given the id
 */
TrackHandler.prototype.get_point = function (id) {
    for (var i=0; i<this.length; i++)
        if (this[i].id == id)
            return this[i]
}

/*
 * TrackHandler#get_point_index ()
 *
 * Returns a point index when id is given
 */
TrackHandler.prototype.get_point_index = function (id) {
    for (var i=0; i<this.length; i++)
        if (this[i].id == id)
            return i
    throw "Couldn't find index " + id + " whithin TrackHandler list."
}


/*
 * TrackHandler#scale_copy
 *
 * Returns a scaled copied array
 */
TrackHandler.prototype.scale_copy = function (scale) {
    var p = []

    scale = scale || 1
    for (var i=0; i<this.length; i++) {
        var el = Object.assign (Object.create(Object.getPrototypeOf (this[i])), this[i])
        el.x *= scale
        el.y *= scale
        p.push (el)
    }
    return p;
}

/*
 * TrackHandler#render
 *
 * PARAMETERS:
 *   n:    A given number whose meaning is determined by option
 *   option: [ "sep" | "points"] Specify if n refers to separation between points or total number of points
 */
TrackHandler.prototype.render = function (n, option) {
    // todo: change this code since is a stub function
    return this.scale_copy()
}

/* TrackHandler#change_class
 *
 * Change the class of a Point in the list.
 *
 * PARAMETERS:
 *   id:                     Id of the point to be changed (not array index)
 *   prop_out: boolean       Wheather this trait mus be present on Class
 *   prop_in:  boolean       Idem
 */
TrackHandler.prototype.change_class = function (point_id, prop_out, prop_in) {
    var i = this.get_point_index(point_id)
    var cl = Point.subclass(prop_out, prop_in)
    var n = new cl()
    this[i] = Object.assign (n, this[i])
}
