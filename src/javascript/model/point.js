/* Class Point: Creates a new Point.
 * The following input is valid:
 *
 *      new Point (2, 7)
 *      new Point (2, 7, 4)
 *      new Point ([2,7])
 *      new Point ({x:2, y:7}, 5)
 *
 * */
function Point() {

    var args = Array.prototype.slice.call(arguments)
    if (!args.length)
        return     // It is an inheritance operation

    this.id = Point.id()
    if (typeof (arguments[0] == "number" || arguments[0] instanceof Number )){
       this.x = args.shift()
       this.y = args.shift()
    }
    if ( arguments[0] instanceof Array ) {
        this.x = args[0][0]
        this.y = args[0][1]
        args.shift()
    }
    if (typeof (arguments[0].x) != "undefined" && typeof (arguments[0].y) != "undefined" ){
        this.x = args[0].x
        this.y = args[0].y
        args.shift()
    }
}

/* Static enumeration */
Point.type = {
    skip: 0,
    tan_out: 1,
    tan_in: 2
}

/* Static variable */

var sbcl = Point.sublcasses = [PointSkip, PointOut, PointIn, PointInOut]
derive_from (sbcl, Point)

Point.id_n = 1

/* Static method */
/*
 * Point.id
 * */
Point.id = function () {
    return Point.id_n++
}

Point.attr = {
    PointOut: {in: false, out: true},
    PointIn:  {in: true, out: false},
    PointInOut:  {in: true, out: true},
    PointSkip:  {in: false, out: false}
}
Point.query = function (attr, subclass) {
    return Point.attr[subclass][attr]
}

Point.subclass = function (pointout, pointin){
    var index = 0
    if (pointout)
        index +=1
    if (pointin)
        index +=2
    return Point.sublcasses[index]
}

/* Methods */

/* Point#add
 *
 * Adds another vector and this one. This results unaffected.
 *
 * PARAMETERS:
 *  p1 : Second addition operator.
 *
 * RETURNS:  new point sum of each.
 * */

Point.prototype.add = function (p1) {
    return new Point (this.x + p1.x, this.y + p1.y)
}


/* Point#subs
 *
 * Substract another vector to this one. This results unaffected.
 *
 * PARAMETERS:
 *  p1 : Second substraction operator.
 *
 * RETURNS:  new point difference of each.
 * */

Point.prototype.subs = function (p1) {
    return new Point (this.x - p1.x, this.y - p1.y)
}

/* Point#scale
 *
 * Scales a vector by a constant. This is unaffected.
 *
 * PARAMETERS:
 *  k : scale factor
 *
 * RETURNS:  new scaled point.
 * */

Point.prototype.scale = function (k) {
    return new Point (k*this.x, k*this.y)
}
