/* class Line */

function Line (/*Point*/ org, /* Point */ end){
    this.org = org;
    this.end = end;
}

/* Line#at
 *
 * Figures out the belonging at a given percentage.
 *
 * PARAMETERS:
 *  pos: Position percentage in line.
 *
 * RETURNS:  Point at requested position.
 * */
Line.prototype.at = function (pos) {
    if (pos < 0 || pos > 1)
        throw "Invalid percentage " + pos
    return this.org.add( (this.end.subs (this.org).scale (pos)  ) )
}
